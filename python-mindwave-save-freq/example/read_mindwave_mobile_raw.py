'''
This code reads the raw values of the brainband, 1 point by second (512 Hz).
For each minute a beep is triggered, at this point the class of the acquisition is changed.
The data starts to be saved after the first beep.
'''
import time
import os
import bluetooth
#from mindwavemobile.development.csv import CSVOutput
from mindwavemobile.MindwaveDataPoints import EEGPowersDataPoint
from mindwavemobile.MindwaveDataPoints import RawDataPoint
from mindwavemobile.MindwaveDataPointReader import MindwaveDataPointReader
import pandas as pd
import textwrap
import datetime
from time import gmtime, strftime

#luz ligado = olho aberto
#'aquisicoes_testes/teste_luz_jose_olhos abertos'
file_name = 'aquisicoes_30_05_19_sem_luz/30_05_19_jose.csv'

flag = 1

# 0 - Classe Aberto
# 1 - Classe Fechado
class_value = 1

if __name__ == '__main__':
    mindwaveDataPointReader = MindwaveDataPointReader('00:15:71:06:E7:78')
    mindwaveDataPointReader.start()
    if (mindwaveDataPointReader.isConnected()): 

        f = open(file_name,'w')
        f.write('rawEEG,time,class\n')
        f.close()

        datetime_object = datetime.datetime.now()
        b = datetime_object + datetime.timedelta(minutes=1) #seconds=59

        while(True):
            dataPoint = mindwaveDataPointReader.readNextDataPoint()
            if (dataPoint.__class__ is RawDataPoint): 
                
                datetime_object = datetime.datetime.now()
                
                if(datetime_object.minute == b.minute):
                    duration = 1  # seconds
                    freq = 440  # Hz
                    os.system('play -nq -t alsa synth {} sine {}'.format(duration, freq))
                    datetime_object = datetime.datetime.now()
                    b = datetime_object + datetime.timedelta(minutes=1)
                    flag = 0
                    class_value = int(not class_value) # It start with this class!

                print(dataPoint)
                print(str(datetime.datetime.now()))

                if(flag == 0):
                    print("salvando...")
                    f = open(file_name,'a')
                    f.write(str(dataPoint.rawValue)+','+str(datetime.datetime.now())+','+str(class_value)+'\n')
                    f.close()
                
    else:
        print(textwrap.dedent("""\
            Exiting because the program could not connect
            to the Mindwave Mobile device.""").replace("\n", " "))   
    