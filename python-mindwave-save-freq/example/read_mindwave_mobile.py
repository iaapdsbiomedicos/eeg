import time, os
import bluetooth
from mindwavemobile.MindwaveDataPoints import EEGPowersDataPoint
from mindwavemobile.MindwaveDataPointReader import MindwaveDataPointReader
import textwrap
import datetime
from time import gmtime, strftime


file_name = 'arquivo_bands.csv'

if __name__ == '__main__':
    mindwaveDataPointReader = MindwaveDataPointReader('00:15:71:06:E7:78')
    mindwaveDataPointReader.start()
    if (mindwaveDataPointReader.isConnected()): 

        f = open(file_name,'w')
        f.write('delta,theta,lowAlpha,highAlpha,lowBeta,highBeta,lowGamma,midGamma,time\n')
        f.close()

        while(True):
            dataPoint = mindwaveDataPointReader.readNextDataPoint()
            if (dataPoint.__class__ is EEGPowersDataPoint):

                print (dataPoint)
                print(datetime.datetime.now())
                
                f = open(file_name,'a')
                f.write(str(dataPoint.delta)+','+str(dataPoint.theta)+','+str(dataPoint.lowAlpha)+','+str(dataPoint.highAlpha)+','+str(dataPoint.lowBeta)+','+str(dataPoint.highBeta)+','+str(dataPoint.lowGamma)+','+str(dataPoint.midGamma)+','+str(datetime.datetime.now())+'\n')
                f.close()
                
    else:
        print(textwrap.dedent("""\
            Exiting because the program could not connect
            to the Mindwave Mobile device.""").replace("\n", " "))
        
