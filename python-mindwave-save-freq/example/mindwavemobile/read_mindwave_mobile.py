import os
import time
import bluetooth
#from mindwavemobile.development.csv import CSVOutput
from MindwaveDataPoints import EEGPowersDataPoint
from MindwaveDataPointReader import MindwaveDataPointReader
import textwrap
from time import gmtime, strftime


if __name__ == '__main__':
    os.environ['TZ'] = 'America/Sao_Paulo'
    time.tzset()
    mindwaveDataPointReader = MindwaveDataPointReader('00:15:71:06:E7:78')
    #mindwaveDataPointReader = MindwaveDataPointReader()
    mindwaveDataPointReader.start()
    if (mindwaveDataPointReader.isConnected()): 
        f = open('file.txt','w')
        f.write('RawEEG,delta,theta,lowAlpha,highAlpha,lowBeta,highBeta,lowGamma,midGamma,time\n')
        f.close()
        while(True):
            dataPoint = mindwaveDataPointReader.readNextDataPoint()
            if (dataPoint.__class__ is EEGPowersDataPoint):
                print(dataPoint) 
                f = open('file.txt','a')
                f.write(str(dataPoint.delta)+','+str(dataPoint.theta)+','
                +str(dataPoint.lowAlpha)+','+str(dataPoint.highAlpha)+','
                +str(dataPoint.lowBeta)+','+str(dataPoint.highBeta)+','
                +str(dataPoint.lowGamma)+','+str(dataPoint.midGamma)+','
                +time.strftime('%d/%m/%Y %X')+'\n')
                f.close()
           
                
    else:
        print(textwrap.dedent("""\
            Exiting because the program could not connect
            to the Mindwave Mobile device.""").replace("\n", " "))
        
